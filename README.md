# Deploy Discourse to Okteto

> **:warning: WORK IN PROGRESS!** Contributors needed, and please proceed with caution.

## Deploy

### One-Click Deploy

[![Develop on Okteto](https://okteto.com/develop-okteto.svg)](https://cloud.okteto.com/deploy?repository=https://gitlab.com/MadeByThePinsHub/boilerplates-okteto/discourse&branch=master)

To update, just run an redeploy from your namespace dashboard.

### Manually with the GUI
